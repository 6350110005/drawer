import 'package:flutter/material.dart';
import 'package:fruitlistview2/model/fruit_data_model.dart';
import 'package:fruitlistview2/pages/fruit_detail.dart';
import 'package:fruitlistview2/pages/main_drawer.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  static List<String> fruitname = ['Apple','Banana','Mango','Orange','Pieapple',];

  static List url = [
    'https://www.applesfromny.com/wp-content/uploads/2020/05/Jonagold_NYAS-Apples2.png',
    'https://cdn.mos.cms.futurecdn.net/42E9as7NaTaAi4A6JcuFwG-1200-80.jpg',
    'https://media.istockphoto.com/photos/mango-isolated-on-white-background-picture-id911274308?k=20&m=911274308&s=612x612&w=0&h=YY8-xqycxsqFea5B-JdhlcgExlXYWMiFoLJdQ-LUx5E=',
    'https://5.imimg.com/data5/VN/YP/MY-33296037/orange-600x600-500x500.jpg',
    'https://5.imimg.com/data5/GJ/MD/MY-35442270/fresh-pineapple-500x500.jpg'
  ];


  static List description = [
    'Apple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Banana is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Mango is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Orange is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
    'Pineapple is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry'
  ];

  final List<FruitDataModel> Fruitdata = List.generate(fruitname.length, (index) =>
      FruitDataModel('${fruitname[index]}', '${url[index]}', '${description[index]}')
  );

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.lightGreen),
              accountName: Text(
                "Natthanon Songsrijan",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 19,
                ),
              ),
              accountEmail: Text(
                "6350110005@email.psu.ac.th",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('lib/assets/IMG_20220718_160420.jpg'),
                radius: 60,
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.home,
              ),
              title: const Text('Home'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.local_dining_rounded,
              ),
              title: const Text('Apple'),
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => FruitDetail(fruitDataModel: Fruitdata[0],)
                    )
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.local_dining_rounded,
              ),
              title: const Text('Banana'),
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => FruitDetail(fruitDataModel: Fruitdata[1],)
                    )
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.local_dining_rounded,
              ),
              title: const Text('Mango'),
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => FruitDetail(fruitDataModel: Fruitdata[2],)
                    )
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.local_dining_rounded,
              ),
              title: const Text('Orange'),
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => FruitDetail(fruitDataModel: Fruitdata[3],)
                    )
                );
              },
            ),
            ListTile(
              leading: Icon(
                Icons.local_dining_rounded,
              ),
              title: const Text('Pineapple'),
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(
                        builder: (context) => FruitDetail(fruitDataModel: Fruitdata[4],)
                    )
                );
              },
            ),
          ],
        ),
      ),
      body: ListView.builder(
          itemCount: Fruitdata.length,
          itemBuilder: (context, index) {
            return Container(
              height: 100,
              child: Card(
                child: Align(
                  alignment: Alignment.center,
                  child: ListTile(
                    title: Text(
                      Fruitdata[index].name,
                      style: TextStyle(
                        fontSize: 25,
                      ),
                    ),
                    leading: SizedBox(
                      width: 70,
                      height: 70,
                      child: Image.network(Fruitdata[index].ImageUrl),
                    ),
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context) => FruitDetail(fruitDataModel: Fruitdata[index],)
                          )
                      );
                    },
                  ),
                ),
              ),
            );
          }
      ),
    );
  }
}
